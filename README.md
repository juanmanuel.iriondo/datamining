# Practica de Big Data de Data Mining

Esta es mi práctica de Data Mining.
El notebook está en el enlace de abajo. En el propio fichero está explicado todo lo que voy haciendo y las respuestas a las preguntas.
Espero haberme explicado bien. Cualquier duda, por favor, me lo comentas.

## Enlace al notebook de Jupyter 0-Practica-DataMining-JuanManuel_Iriondo.ipynb

https://drive.google.com/file/d/1m7e63yxB-XnnuzINggZBgpyEAH4QDekF/view?usp=sharing
